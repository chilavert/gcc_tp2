El sistema de autenticacion fue realiado utilizando la herramienta nodeJs en su versión 11.0.0. Los datos de los usuarios registrados son almacenados en un vector de objetos, para mayor simplicidad.

Para el trabajo son utilizados dos ramas de trabajo, una raiz master y una rama develop.

Para realizar el proceso de integracion continua, primeramente se establece el archivo gitlab-ci.yml. Dentro de el se establecen los tres stages que serán ejecutados:
-Desarrollo
-Homologación (test)
-Produccion


**Stage de Homologación:**

    Para llevar a cabo este stage se establecieron dos pruebas unitarias sobre el modulo de login. Para las pruebas fue utilizado el framework mocha, el cual ofrece soporte de navegador, pruebas asíncronas, informes de cobertura de prueba y uso de cualquier biblioteca de aserciones.
    El script necesario para ejecutar estos test consiste del comando **"npm test"**
    
    
**Stage de Desarrollo:**

    Luego de registrar un usuario en la plataforma de Heroku, se procede a crear dos aplicaciones, una denominada **gcctp2-staging** y otra **gcctp2-production**.
    El stage de desarrollo es dividido en dos trabajos, uno para el trabajo de staging y el otro para production. El trabajo de staging es especificado de uso solo para la rama develop.
    En ambos casos se instala la dependencia dpl necesaria mediante el comando **gem install dpl** y seguidamente se procede a deployar la aplicacion en heroku especificando el nombre de la aplicacion y la variable key definida previamente.
    
 **Stage de Producción:**
 
    Mediante las configuraciones previas en gitLab CI es súper útil para implementar los cambios antes de la producción y finalmente el entorno de producción.

